<?php

return array(
    

    'user/register' => 'user/register',

    'user/logout' => 'user/logout',

    'site'=>'site/index',

    'messages/refresh' => 'messages/refresh',

    'id([0-9]+)'=>'profile/index/$1',

    'messages'=>'messages/index',

    'friends' => 'friends/index',

    '' => 'user/login', // actionIndex в SiteController
    
);