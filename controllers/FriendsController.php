<?php

/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.11.2016
 * Time: 0:23
 */
class FriendsController
{
    public static function actionIndex(){
        if(!User::Logedin()) header("Location:/");
        $strfriends=User::getFriendsByUser($_SESSION['user']['id']);
        if($strfriends){
            $arrfriends= unserialize(base64_decode($strfriends));
            $friends=User::getInfoFriends($arrfriends);
        }
        require_once ROOT.'/views/friends/friends.php';
        return true;
    }
}