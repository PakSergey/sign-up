<?php

/**
 * Created by PhpStorm.
 * User: Сергей
 * Date: 01.11.2016
 * Time: 0:46
 */
class MessagesController
{
    public static function actionIndex(){
        if(!User::Logedin()) {header("Location:/");exit();};

        if(isset($_POST['submit'])){
            $friendid=$_POST['friend'];
            $friend=User::getUserById($friendid);
            $_SESSION['friend']=$friend;
            header('Location:/messages');
        }
        if(isset($_POST['send'])){
            $message=$_POST['message'];
            $id=Site::sendMessage($message);
            header('Location:/messages');
        }
        if(isset($_SESSION['friend'])) {
            $friend = $_SESSION['friend'];
            $idmessages = Site::getAllMessages($_SESSION['user']['id']);
            $messages = array();
            if (!empty($idmessages)) {
                $i=0;
                foreach ($idmessages as $message) {

                    $temp=Site::getMessageById($message);
                    $messages[$i]['message'] =$temp;
                    $messages[$i]['author'] = User::getUserById($temp['author_id']);
                    $i++;
                }
            }
        }

        require_once ROOT.'/views/messages/messages.php';
        return true;
    }

    public function actionRefresh(){
        $idmessages = Site::getAllMessages($_SESSION['user']['id']);
        $messages = array();
        if (!empty($idmessages)) {
            $i=0;
            foreach ($idmessages as $message) {

                $temp=Site::getMessageById($message);
                $messages[$i]['message'] =$temp;
                $messages[$i]['author'] = User::getUserById($temp['author_id']);
                $i++;
            }
        }

    if(!empty($messages) && is_array($messages))
    foreach ($messages as $message):
       echo  "<p>".$message['author']['firstname'].': '.$message['message']['text']."</p>";
    endforeach;
        return true;
    }
}