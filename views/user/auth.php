<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SignUP</title>
    <meta name="description" content="SignUP" />
    <meta name="robots" content="index, follow">
    <link rel="stylesheet" type="text/css" href="/template/css/style.css">
    <link rel="stylesheet" type="text/css" href="/template/css/auth.css">
</head>
<body>
    <div class="authblock">
        <h1>Введите свой Логин и Пароль для входа!</h1>
        <form action="" method="POST">
            <table>
                <tr>
                    <td><input type="text" name="email" placeholder="Введите E-mail"></td>
                </tr>
                <tr>
                    <td><input type="password" name="password" placeholder="Введите пароль"></td>
                </tr>
            </table>
            <div id="cellinput">
                <input type="submit" value="Войти" name="auth">
                <input type="submit" value="Зарегестрироваться" name="reg">
            </div>
        </form>
    </div>
</body>
</html>