<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>SignUP</title>
    <meta name="description" content="SignUP" />
    <meta name="robots" content="index, follow">
    <link rel="stylesheet" type="text/css" href="/template/css/style.css">
    <link rel="stylesheet" type="text/css" href="/template/css/reg.css">
</head>
<body>
    <div class="regblock">
        <h1>Заполните форму регистрации!</h1>
        <form action="" method="POST">
            <table>
                <tr>
                    <?php
                        if(!empty($errors) && is_array($errors))
                        foreach ($errors as $error){
                            echo $error.'<br>';
                        }
                    ?>
                </tr>
                <tr>
                    <td><p>Имя:</p></td>
                    <td><input type="text" name="firstname" value="<?php echo $firstname;?>" required></td>
                </tr>
                <tr>
                    <td><p>Фамилия:</p></td>
                    <td><input type="text" name="lastname" value="<?php echo $lastname;?>" required></td>
                </tr>
                <tr>
                    <td><p>Дата рождения:</p></td>
                    <td><input type="date" name="birthdate" max="2008-01-01" min="1950-01-01" name="date" value="<?php echo $birthdate;?>" required></td>
                </tr>
                <tr>
                    <td><p>Город:</p></td>
                    <td><input type="text" name="city" value="<?php echo $city;?>" required></td>
                </tr>
                <tr>
                    <td><p>Пол:</p></td>
                    <td>
                        <label><input type="radio" name="gender" value="male" required/> Мужской</label> <label><input type="radio" name="gender" value="female" required/>Женский</label>
                    </td>
                </tr>
                <tr>
                    <td><p>Почта:</p></td>
                    <td><input type="email" name="email" value="<?php echo $email;?>" required></td>
                </tr>
                <tr>
                    <td><p>Пароль:</p></td>
                    <td><input type="password" name="password" required></td>
                </tr>
            </table>
            <div class="regbutton">
                <input type="submit" name=register value="Зарегестрироваться">
            </div>
        </form>

    </div>
</body>
</html>